package com.desk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessagePayloadsWithThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessagePayloadsWithThymeleafApplication.class, args);
	}

}
